# Notes

## Follow-Ups

* [ ] what are raw strings?
* [ ] [read the recap on how to evaluate `__class__` attributes](https://github.com/gregmalcolm/python_koans/wiki/Class-Attribute) 

## `about_lists`

- Note, popping from the left hand side of a list is inefficient. Use `collections.deque` instead.
